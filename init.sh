#!/usr/bin/env bash

echo "Starting initialization"

LOGFILE="osxInit.log"

function installHomebrew() {
    if [[ `which brew` ]]
        then
            echo "Homebrew is already installed" |tee -a $LOGFILE
        else
            echo "Installing Homebrew" |tee -a $LOGFILE
            /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" 2>&1>$LOGFILE
    fi
}

function installPython3() {
    if [[ `which python3` ]]
        then
            echo "Python3 is already installed" |tee -a $LOGFILE
        else
            echo "Installing Python3 and pip" |tee -a $LOGFILE
            brew install python3 2>&1>$LOGFILE
            brew install pip 2>&1>$LOGFILE
    fi
}

function installGolang() {
    if [[ `which go` ]]
        then
            echo "golang is already installed" |tee -a $LOGFILE
        else
            echo "Installing golang" |tee -a $LOGFILE
            brew install golang 2>&1>$LOGFILE
    fi
}

function installAnsible() {
    if [[ `which ansible` ]]
        then
            echo "ansible is already installed" |tee -a $LOGFILE
        else
            echo "Installing golang" |tee -a $LOGFILE
            pip install ansible 2>&1>$LOGFILE
    fi
    #/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
}

if [[ -f $LOGFILE ]]
    then
        echo "" >> $LOGFILE
        echo `date` >> $LOGFILE
    else
        touch $LOGFILE
        echo `date` >> $LOGFILE
fi

installHomebrew
installPython3
installGolang
installAnsible

echo "Script complete. Please review $LOGFILE for any errors"